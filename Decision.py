# This piece of code contains the algorithms necessary or the creations of playlists or suggestions based on 
# the features extracted by the analysis script
import essentia
import essentia.standard
import essentia.streaming
from essentia.standard import *

import musdb

import scipy
import librosa
import soundfile
from IPython.display import Audio, display

import matplotlib.pyplot as plt
import numpy as np
import os

import IPython
from pylab import *

import timeit

import torch
from torch import nn,optim
from torch.nn import functional as F
from torch.autograd import Variable
import torchvision.models as models
from torchvision import transforms
from torch.distributions import kl as kl
from fastai.basics import *
from fastai.vision import *

from sklearn import metrics 
from sklearn.metrics import roc_auc_score
from sklearn.cluster import DBSCAN

import pandas as pd
import time
from tqdm import tqdm

import sys
import ipdb

sys.path.insert(1, '../Analysis/Models/open_unmix_pytorch/')
from tiesto import separate as sep

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

#features=pd.read_pickle('../Analysis/files/FEATURES_SS_LL_EMO')
features=pd.read_pickle('../Analysis/files/FEATURES_SS_LL_EMO_v2')



def compute_naive_similarity(distr1,distr2):
    '''
    this function outputs the similarity of two distributions, according to L1 norm
    '''
    a=distr1[0]
    b=distr1[1]
    c=distr2[0]
    d=distr2[1]
    similarity=0
    #a & c are means, b & d are stds of hit distributiona
    for i in range(len(a)):
        x0min=a[i]-abs(b[i])
        x0max=a[i]+abs(b[i])
        x1min=c[i]-abs(d[i])
        x1max=c[i]+abs(d[i])
    
        if x0max<x1max:
            similarity+=x0max-x1min
        if x1max<x0max:
            similarity+=x1max-x0min

    return similarity
def name_from_path(str):
    '''
    Returns name from path, usefule for visualization
    '''
    name=''
    slashvec=[]
    for i in range(len(str)):
        if str[i]=='/':
            slashvec.append(i)
    name=str[slashvec[-1]+1:-4]
    return name

def build_rand_list_SS_bpm(length,first_id,similarity_func=compute_naive_similarity):
    '''
    This is the funtion that returns a playlist
    '''
    
    ##starting from a random track
    #first_id=torch.randint(0, len(features)-1,(1,)).numpy()[0]
    
    first_name=features['Name'][first_id]
    first_bpm=features['BPM'][first_id]
    first=features['Deep Hits'][first_id]
    first_diss=features['Dissonance'][first_id]
    first_comp=features['Dynamic Complexity'][first_id]
    first_aro=features['Arousal'][first_id]
    print(name_from_path(first_name))
    
    vocalsims=[]
    drumsims=[]
    basssims=[]
    othersims=[]
    not_in_range=0
    
    for i in tqdm(range(len(features))):
        #All low level constraints are imposed here
        current_bpm=features['BPM'][i]
        
        verif=first_diss-features['Dissonance'].std()<features['Dissonance'][i]<first_diss+features['Dissonance'].std() and first_comp-features['Dynamic Complexity'].std()<features['Dynamic Complexity'][i]<features['Dynamic Complexity'].std()+first_comp and (0.92*current_bpm<first_bpm<1.08*current_bpm or 1.84*current_bpm<first_bpm<2.16*current_bpm or 0.46*current_bpm<first_bpm<0.54*current_bpm)
        
        amb_check=first_aro-features['Arousal'].std()<features['Arousal'][i]<first_aro+features['Arousal'].std() # replace this by True if not enough sounds are present
        
            
        if verif and amb_check:
            #Here, the comparison o the deep projections is made
                 
            comparison=features['Deep Hits'][i]
            
            try:
                vocalsims.append(similarity_func(first[0],comparison[0]))
                drumsims.append(similarity_func(first[1],comparison[1]))
                basssims.append(similarity_func(first[2],comparison[2]))
                othersims.append(similarity_func(first[3],comparison[3]))
            except:
                print(features['Name'][i],"Could not  be processed")
                vocalsims.append(torch.as_tensor([0]))
                drumsims.append(torch.as_tensor([0]))
                basssims.append(torch.as_tensor([0]))
                othersims.append(torch.as_tensor([0]))
        else:
            vocalsims.append(torch.as_tensor([0]))
            drumsims.append(torch.as_tensor([0]))
            basssims.append(torch.as_tensor([0]))
            othersims.append(torch.as_tensor([0]))
            not_in_range+=1
            
        
            
    vocalsims=np.array(vocalsims) 
    drumsims=np.array(drumsims)
    basssims=np.array(basssims)
    othersims=np.array(othersims)
    sims=torch.as_tensor([[vocalsims],[drumsims],[basssims],[othersims],[vocalsims+drumsims+basssims+othersims]])
    sims=sims.view(sims.size(0),sims.size(2))
    simsum=sims.sum(0)
                                  
    print("Not in constraints:",not_in_range)
    print(50*'*')
    
### taking the first song as a reference for all similarities is worse (in a musical sense) but much more efficient, computationally.  
    playlist=[name_from_path(features['Name'][first_id])]
    for k in range(sims.size(0)):
    
        for j in range(length):                       
            
            next=torch.argmax(sims[k]) #best playlist
            #print('Similarity score',sims[k,next])
            sims[k,next]=0
            
            playlist.append(name_from_path(features['Name'][int(next.numpy())]))
        #playlist.append('||')
    
    return playlist

def get_rid_of_doubles(playlist):
    '''
    Some times duplicates appear, removes them
    '''
    newlist=[]
    for i in range(len(playlist)):
        if playlist[i] not in newlist:
            newlist.append(playlist[i])
    return newlist

def find_emo_vect(track_name):
    '''
    finds AV vector
    '''
    emo_vect=[999,999]
    for i in range(features['Name'].size):
        if str(track_name) in str(features['Name'][i]) or str(features['Name'][i]) in str(track_name):
            emo_vect=[features['Valence'][i],features['Arousal'][i]]
            
    return emo_vect

def find_10_closest(emo_scores,length):
    scores=[]
    closest_ten=[]
    for i in range(len(emo_scores)):
        all_dists=[]
        
        for j in range(len(emo_scores)):
            
            all_dists.append(np.sqrt(np.square(emo_scores[i][0]-emo_scores[j][0])+np.square(emo_scores[i][1]-emo_scores[j][1])))
        all_dists=np.array(all_dists)
        closest_ten.append(all_dists.argsort()[0:length-1])
        scores.append(all_dists[all_dists.argsort()[0:length-1]].sum())
        
    return closest_ten[np.array(scores).argmin()]

def Build_SS_Emo_playlist(length,first_id):
    '''
    Builds a more complex playlist, with danceable order
    '''
    
    playlist=build_rand_list_SS_bpm(length,first_id)
    playlist=get_rid_of_doubles(playlist)
    print("The playlist ordered by danceability scores")
    
    playlist=danceable_order(playlist)
    emo_scores=[]
    for i in range(len(playlist)):
        emo_scores.append(find_emo_vect(playlist[i]))
    best_playlist_id=find_10_closest(emo_scores,length)
    c=[]
    for j in range(len(best_playlist_id)):
        
        print(j,'. ',playlist[best_playlist_id[j]])
        c.append(playlist[best_playlist_id[j]])
        
    return c

def danceable_order(playlist):
    '''
    defines the danceable order of playlist
    '''
    dance_mat=[]
    for i in range(len(playlist)):
        for j in range(features['Name'].size):
            if str(playlist[i]) in str(features['Name'][j]) or str(features['Name'][j]) in str(playlist[i]):
                dance_mat.append(features['Danceability'][j])
                break
    dance_sort=np.argsort(dance_mat)
    #print(playlist)
    #print(dance_sort)
    
    return np.array(playlist)[dance_sort]

def danceability_score(music1,music2):
    '''
    returns the score needed for danceable order
    '''
    for i in range(features['Name'].size):
        if str(music1) in str(features['Name'][i]) or str(features['Name'][i]) in str(music1):
            d1=features['Danceability'][i]
        if str(music2) in str(features['Name'][i]) or str(features['Name'][i]) in str(music2):
            d2=features['Danceability'][i]
    score=1/np.sqrt(abs(d1-d2))
    return score

def harmonicity(music1,music2):
    '''
    This function checks if two pieces are compatible harmonically
    '''
    for i in range(features['Name'].size):
        if features['Scale'][i]=='C#':
            features['Scale'][i]='Db'
        if str(music1) in str(features['Name'][i]) or str(features['Name'][i]) in str(music1):
            k1=[features['Scale'][i],features['Key'][i]]
        if str(music2) in str(features['Name'][i]) or str(features['Name'][i]) in str(music2):
            k2=[features['Scale'][i],features['Key'][i]]
    
    harmony_matrix=[['B','F#','Db','Ab','Eb','Bb','F','C','G','D','A','E'],['Ab','Eb','Bb','F','C','G','D','A','E','B','F#','Db']]
    compatibility_list=[]
    for j in range(len(harmony_matrix[0])):
        if k1[1]=='major':
            if k1[0]==harmony_matrix[0][j]:
                if j==0:
                    k=11
                    l=1
                if j==11:
                    k=10
                    l=0
                else:
                    k=j-1
                    l=j+1


                compatibility_list.append([harmony_matrix[0][j],'major',harmony_matrix[1][j],'minor',harmony_matrix[0][k],'major',harmony_matrix[0][l],'major'])
        
        if k1[1]=='minor':
            if k1[0]==harmony_matrix[1][j]:
                if j==0:
                    k=11
                    l=1
                if j==11:
                    k=10
                    l=0
                else:
                    k=j-1
                    l=j+1

    
                compatibility_list.append([harmony_matrix[1][j],'minor',harmony_matrix[0][j],'major',harmony_matrix[1][k],'minor',harmony_matrix[1][l],'minor'])
    x=0
    try:
        for m in range(len(compatibility_list[0])-1):
        
        
            if str(k2[0]+k2[1])==str(compatibility_list[0][m]+compatibility_list[0][m+1]):
                x=1
    except:
        pass
    
    
    return x

def index_from_name(music_name):
    for i in range(len(features)):
        if str(music_name) in str(features['Name'][i]) or str(features['Name'][i]) in str(music_name):
            x=i
    return x

def create_connect_matrix_harmonic(playlist): 
    '''
    Returns harmonic compability matrix within a playlist
    This is a NP-hard problem: https://en.wikipedia.org/wiki/Longest_path_problem
    '''
    
    connect_matrix=[]
    for i in range(len(playlist)):
        connect_vect=[]
        for j in range(len(playlist)):
            connect_vect.append(harmonicity(playlist[i],playlist[j]))
        connect_matrix.append(connect_vect)
    for i in range(len(connect_matrix)):
        connect_matrix[i][i]=0
    
    
    return connect_matrix

def harmonic_subsets(playlist):
    '''
    Returns harmonicallly playable subsets of a playlist
    '''
    con_matrix=create_connect_matrix_harmonic(playlist)
    used_vec=np.zeros(len(playlist))
    opt_playlist=[]
    for i in range(len(playlist)):
        opt_vec=[]
        cop_vec=used_vec
        cop_vec[i]=1
        cop_matrix=con_matrix
        itera=0
        base=i
        for n in range(len(con_matrix[i])):
            if con_matrix[i][n]==1:
                itera+=1
                
        for t in range(itera):
            i=base
            
            for j in range(len(con_matrix[i])):
                if cop_matrix[i][j]==1 and cop_vec[j]==0:
                    cop_vec[j]=1
                    
                    nextt=j
                    opt_vec.append(i)
                    opt_vec.append(j)
                    
                    cop_matrix[j][i]=0
                    cop_matrix[i][j]=0
                    
                    i=j
                    j=0
            
            opt_playlist.append(opt_vec)
    a=0
    index=0
    
    
    for k in range(len(opt_playlist)):
        opt_playlist[k]=get_rid_of_doubles(opt_playlist[k])
    
    
    
    
    for k in range(len(opt_playlist)):
        
        if len(opt_playlist[k])>a:
            a=len(opt_playlist[k])
            index=k
    print('The longest harmonic playlist is of length:',a)
    
    new_playlist=[]
    opt_playlist=get_rid_of_doubles(opt_playlist)
    dancescore=[]
    #Here we sort the playlists by sorting harmonic subsets by danceability
    for h in range(len(opt_playlist)):
        dance=0
        for g in range(len(opt_playlist[h])):
            for j in range(features['Name'].size):
                if str(playlist[opt_playlist[h][g]]) in str(features['Name'][j]) or str(features['Name'][j]) in str(opt_playlist[h][g]):
                    dance+=features['Danceability'][j]
                    
        dancescore.append(dance)
        
    sort=np.argsort(dancescore)
    opt_playlist=np.array(opt_playlist)[sort]
    
    for a in range(len(opt_playlist)):
        dance=0
        for s in range(len(opt_playlist[a])):
            new_playlist.append(playlist[opt_playlist[a][s]])
    
    missing=[] 
    ha=0
    for s in range(len(playlist)):
        
        for t in range(len(new_playlist)):
            if str(playlist[s]) in str(new_playlist[t]):
                ha=1
        if ha==0:
            missing.append(playlist[s])
        ha=0
    for m in range(len(missing)):
        new_playlist.append(missing[m])
    
        
    for j in range(len(new_playlist)):
        print(j,'. ',new_playlist[j])
    #for m in range(len(opt_playlist[index])):
        #new_playlist.append(playlist[opt_playlist[index][m]])            
                
                
    return new_playlist
                
             
        

        
def dance_emo_harmonic_mixable_playlist(length,first_id):
    '''
    Combines the above to yield the final playlists
    '''
    playlist=Build_SS_Emo_playlist(length,first_id)
    print(50*'*')
    playlist=harmonic_subsets(playlist)
    
    
    return playlist
    
    
def start_from_track(trackname):
    '''
    Want to start from a track? use this function
    '''
    try:
        for i in range(features['Name'].size):
            if str(trackname) in name_from_path(str(features['Name'][i])) or name_from_path(str(features['Name'][i])) in str(trackname):
                first_name=name_from_path(features['Name'][i])
                first_bpm=features['BPM'][i]
                first_diss=features['Dissonance'][i]
                first_dyn=features['Dynamic Complexity'][i]
                first_id=i
    except:
        print("The requested track was not found in the collection")
    return first_id,first_name,first_bpm,first_diss,first_dyn

def start_from_parameters(dissonance,complexity,bpm):
    '''
    This returns a starting track, if starting from parameters
    '''
    dissonance=dissonance + 0.01*(np.random.random(1)-0.5)
    complexity=complexity + 0.5*(np.random.random(1)-0.5)
    bpm=bpm + 2*(np.random.random(1)-0.5)
    potential_starters=[]
    for i in range(features['Name'].size):
        if 0.92*bpm<features['BPM'][i]<1.08*bpm:
            if -0.01+dissonance<features['Dissonance'][i]<0.01+dissonance:
                if -0.5+complexity<features['Dynamic Complexity'][i]<0.5+complexity:
                    potential_starters.append(features['Name'][i])
    try:
        first=torch.randint(0, len(potential_starters)-1,(1,)).numpy()[0]
        first_id,first_name,first_bpm,first_diss,first_dyn=start_from_track(potential_starters[first])
    except:
        print('You do not have any tracks matching the requested parameters')
    return first_id,first_name,first_bpm,first_diss,first_dyn
        
###*** Only use these if you want to generate playlists. ***###

#first_id=torch.randint(0, len(features)-1,(1,)).numpy()[0]
first_id,_,_,_,_=start_from_parameters(0.43,3,110)
#first_id,_,_,_,_=start_from_track('Adriani')
dance_emo_harmonic_mixable_playlist(15,first_id)       
    
        
       
        
            
        

    
