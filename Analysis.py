## This piece of code is responsible  for the analysis of audio files. from a raw .wav or .mp3 file, ##
## it computes the MEL spectrogramm, extract low-level to high-level features and stores them into a ##
## pandas dataframe, which will be used for playlist generation.                                     ##



import essentia
import essentia.standard
import essentia.streaming
from essentia.standard import *

import musdb

import scipy
import librosa
import soundfile
from IPython.display import Audio, display

import matplotlib.pyplot as plt
import numpy as np
import os

import IPython
from pylab import *

import timeit

import torch
from torch import nn,optim
from torch.nn import functional as F
from torch.autograd import Variable
import torchvision.models as models
from torchvision import transforms
from torch.distributions import kl as kl
from fastai.basics import *
from fastai.vision import *

from sklearn import metrics 
from sklearn.metrics import roc_auc_score
from sklearn.cluster import DBSCAN

import pandas as pd
import time
from tqdm import tqdm

import sys
import ipdb

sys.path.insert(1, 'Models/open_unmix_pytorch/')
from tiesto import separate as sep

use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else "cpu")

path='/opt/datasets/bcyolek_sound_v1' #Input the path of your master music directory



def name_from_path(str):
    '''
    Gets the name from a piece from its path
    '''
    
    name=''
    slashvec=[]
    for i in range(len(str)):
        if str[i]=='/':
            slashvec.append(i)
    name=str[slashvec[-1]+1:-4]
    return name

def list_files(dir):
    '''
    Creates a vector pointing to all music files
    '''
    r = []                                                                                                            
    subdirs = [x[0] for x in os.walk(dir)]                                                                            
    for subdir in subdirs:                                                                                            
        files = os.walk(subdir).__next__()[2]                                                                             
        if (len(files) > 0):                                                                                          
            for file in files:                                                                                        
                r.append(subdir + "/" + file)                                                                         
    return r 


F_list=list_files(path)





try: #see if the database and music folders are synchronized
    #old_features=pd.read_pickle('files/FEATURES_SS_LL_EMO')
    old_features=pd.read_pickle('files/FEATURES_SS_LL_EMO_v2')#v2 is with slower but better extractor
    names=[]
    for i in range(len(old_features['Name'])):
        if os.path.exists(old_features['Name'][i])==False:
            names.append(name_from_path(str(old_features['Name'][i])))
            old_features.drop(old_features.index[i])
            old_features=old_features[old_features.index != i]
    old_features = old_features.reset_index(drop=True)
    giter=old_features['Name'].size
    if len(names)!=0:
        print("The followings songs were not found in your collection and haved been removed from the database:",names)
except:
    giter=0
    


doublons_idx=[]


for i in range(len(F_list)):
    for j in range(giter):
        if str(F_list[i]) in str(old_features['Name'][j]) or str(old_features['Name'][j]) in str(F_list[i]):
            doublons_idx.append(i)
F_list
newlist=[]
for i in range(len(F_list)):
    
    
    if i not in doublons_idx:
        
        if F_list[i].endswith((".mp3") or ('.wav')):
            newlist.append(F_list[i])
    
        
F_list=newlist
        
print(F_list) 




            
        


######################################## LOW-LEVEL-FEATURE EXTRACTION ##########################################################

#This function extracts meaningful low-level features as well as the mel spectrogramm, which will be used as input for the deep convolutional neural networks 
def list_files(dir):                                                                                                  
    r = []                                                                                                            
    subdirs = [x[0] for x in os.walk(dir)]                                                                            
    for subdir in subdirs:                                                                                            
        files = os.walk(subdir).__next__()[2]                                                                             
        if (len(files) > 0):                                                                                          
            for file in files:                                                                                        
                r.append(subdir + "/" + file)                                                                         
    return r 

def faster_extractor(path):
    '''
    captures only beat positions, extracts low level features and beat positions
    '''
    
    
    features, features_frames = essentia.standard.MusicExtractor(lowlevelStats=['mean'],
                                              rhythmStats=['mean'],
                                              tonalStats=['mean'])(path)
    
    return features['rhythm.bpm'],features['rhythm.danceability'],features['tonal.key_edma.scale'],features['tonal.key_edma.key'],features['lowlevel.dynamic_complexity'],features['lowlevel.zerocrossingrate.mean'],features['lowlevel.dissonance.mean'],features['lowlevel.dissonance.mean'],features['rhythm.beats_position']

def slow_extractor(path):
    '''
    extracts low level features and all onset positions
    '''
    features, features_frames = essentia.standard.MusicExtractor(lowlevelStats=['mean'],
                                              rhythmStats=['mean'],
                                              tonalStats=['mean'])(path)
    audio = MonoLoader(filename=path)()
    od = OnsetDetection(method='complex')
    # Let's also get the other algorithms we will need, and a pool to store the results
    w = Windowing(type = 'hann')
    fft = FFT() # this gives us a complex FFT
    c2p = CartesianToPolar() # and this turns it into a pair (magnitude, phase)
    pool = essentia.Pool()
    
    #Computing onset detection functions.
    for frame in FrameGenerator(audio, frameSize = 1024, hopSize = 512):
        mag, phase, = c2p(fft(w(frame)))
        pool.add('features.complex', od(mag, phase))
    
    onsets = Onsets()
    onsets_complex = onsets(essentia.array([ pool['features.complex'] ]), [ 1 ])    
    return features['rhythm.bpm'],features['rhythm.danceability'],features['tonal.key_edma.scale'],features['tonal.key_edma.key'],features['lowlevel.dynamic_complexity'],features['lowlevel.zerocrossingrate.mean'],features['lowlevel.dissonance.mean'],features['lowlevel.dissonance.mean'],onsets_complex



    

def analyze_directory_ll(path, extractor=slow_extractor):
    '''
    extracts the low level features from a master directory,
    '''
    directory = os.fsencode(path)
    noms=[]
    bpm=[]
    danceability=[]
    key=[]
    scale=[]
    dyn_complexity=[]
    zero_crossing=[]
    dissonance=[]
    p_salience=[]
    beat_pos=[]
    
    
    for k in tqdm(range(len(f_list))):
        try:
            
            newpath=f_list[k]
            BPM,DANCE,KEY,SCALE,DYN,ZERO,DISS,PSAL,BPOS=extractor(newpath)
            noms.append(f_list[k])
            
            bpm.append(BPM)
            danceability.append(DANCE)
            key.append(KEY)
            scale.append(SCALE)
            dyn_complexity.append(DYN)
            zero_crossing.append(ZERO)
            dissonance.append(DISS)
            p_salience.append(PSAL)
            beat_pos.append(BPOS)
            
            
        except:
            print('ERROR:',f_list[k])
            pass
        
    
    FEATS=pd.DataFrame({'Name':noms,'BPM':bpm,'Danceability':danceability,'Key':key,'Scale':scale,'Dynamic Complexity':dyn_complexity,'Zero-crossing rate':zero_crossing,'Dissonance':dissonance,'Pitch Salience':p_salience,'Beat Positions':beat_pos})
    
    return FEATS






################################## Deep Models for high level processing
class mod_resnet(nn.Module):
    def __init__(self):
        super().__init__()
        self.lin=nn.Linear(1000,24)
        self.Resnet50=models.resnet18()
        self.Resnet50.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.softmax=nn.Softmax(dim=0)
        
    def forward(self,x):
        x=x.unsqueeze(1)
        x=self.Resnet50(x)
        #x=self.dropout(x)
        x=self.lin(x)
        
        
        return x
print('Loading 1st deep model...')
supermodele=mod_resnet()
supermodele.load_state_dict(torch.load('Models/deep_models/res18_fully_trained_dic.pt'))
print('1st loaded!')  
supermodele.to(device)
class Identity(nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x
    
    
class rep_net(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.supermod=supermodele
        
        
        self.supermod.lin = nn.Identity()
        self.softmax=nn.Softmax()
        for param in self.supermod.parameters():
            param.requires_grad = False
            
    def forward(self,x):
        
        x=self.supermod(x)
        
        
        return x
    
print('Loading second deep models...')
rep_net_trained = rep_net()
rep_net_trained.load_state_dict(torch.load('Models/deep_models/synth_rep_netv2.pt'))
rep_net_trained.eval()
rep_net_trained.to(device)

print('All models loaded!')

###################################### COMPUTING HIGH LEVEL FEATURES ############################################

def compute_mel(monoaudio):
    '''
    This function computes a mel-spectrogram from an audio track
    '''

    audio = monoaudio
    #hyperparameters
    w = Windowing(type = 'hann')
    spectrum = Spectrum()  # FFT() would return the complex FFT, here we just want the magnitude spectrum
    mfcc = MFCC(numberCoefficients=40) #10 works also
    logNorm = UnaryOperator(type='log')

    mfccs = []
    melbands = []
    melbands_log = []

    for frame in FrameGenerator(audio, frameSize=2206, hopSize=1103, startFromZero=True):
        mfcc_bands, _ = mfcc(spectrum(w(frame)))
        
        melbands.append(mfcc_bands)
        melbands_log.append(logNorm(mfcc_bands))

    # transpose to have it in a better shape
    # we need to convert the list to an essentia.array first (== numpy.array of floats)
    #melbands are not used in the beginning but can be useful
    
    melbands_log = essentia.array(melbands_log).T
    
    return melbands_log

def extract_all_hits(audiofile,audio):
    '''
    This function returns all spectrograms for all detected note events
    '''
    audiofile=os.path.abspath(audiofile)
    
    all_hits=[]
    mels=compute_mel(audio)
    
    for k in range(len(LOW_LEVEL_FEATURES)):
        if audiofile in os.path.abspath(str(LOW_LEVEL_FEATURES['Name'][k])):
            beatpos=LOW_LEVEL_FEATURES['Beat Positions'][k]
            break
   
    
    
   
    for i in range(len(beatpos)):
        framepos=int(round(beatpos[i]/0.025))
        if framepos+157<mels.shape[1]:
            all_hits.append(mels[:,framepos:framepos+157])
            

    return all_hits
############################################################# #############################################################
############################################################# #############################################################
    
def deep_rep(hits,model):
    '''
    This function returns the deep represenations, given the extracted hits
    '''
    deep_hits=[]
    for i in range(len(hits)):
        hitrep=model(torch.as_tensor(hits[i:i+1]).to(device))
        hitrep=hitrep.view(1000).detach().cpu().numpy()
        deep_hits.append(hitrep)
    return torch.as_tensor(deep_hits )  


def preprocess_dir_hits(dir):
    '''
    In this function, a directory is processed and the deep hits are returned
    '''
    #ALL_hits=[]
    deephits=[]
    
    #f_list=list_files(dir)
    
    
    for k in range(len(f_list)):
                
        
            if f_list[k].endswith((".mp3") or ('.wav')) :
                
                try:
                    t0=time.time()
                    
                    audio,_=librosa.load(f_list[k], sr=44100, mono=False)
                    estimates = sep(audio=audio.T, targets=['vocals', 'drums', 'bass', 'other'], device=device, residual_model=False)
                    print(time.time()-t0)
                    hits1=extract_all_hits(f_list[k],essentia.array((estimates['vocals'].T[0]+estimates['vocals'].T[1])/2))
                    hits2=extract_all_hits(f_list[k],essentia.array((estimates['drums'].T[0]+estimates['drums'].T[1])/2))
                    hits3=extract_all_hits(f_list[k],essentia.array((estimates['bass'].T[0]+estimates['bass'].T[1])/2))
                    hits4=extract_all_hits(f_list[k],essentia.array((estimates['other'].T[0]+estimates['other'].T[1])/2))
                    print(time.time()-t0)
                    d_hits1=deep_rep(hits1, rep_net_trained).detach().cpu()
                    d_hits2=deep_rep(hits2, rep_net_trained).detach().cpu()
                    d_hits3=deep_rep(hits3, rep_net_trained).detach().cpu()
                    d_hits4=deep_rep(hits4, rep_net_trained).detach().cpu()
                    print(time.time()-t0)
                    
                    dh1=[d_hits1.mean(0),d_hits1.std(0)]
                    dh2=[d_hits2.mean(0),d_hits2.std(0)]
                    dh3=[d_hits3.mean(0),d_hits3.std(0)]
                    dh4=[d_hits4.mean(0),d_hits4.std(0)]
                    deephits.append([dh1,dh2,dh3,dh4])
                    print('Total Time',time.time()-t0)
                    print(k+1,'/',len(f_list))
                
                
                    
                
                except:
                    print('ERROR:',f_list[k])
                    dh=torch.Tensor(np.full((1000),1000))
                    deephits.append([dh,dh,dh,dh])
                    pass
    
    
                    
                
                
                
        
            
    return deephits




########################################### AROUSAL-VALENCE MOOD MAPPING ##############################################
class ValenceModel2(nn.Module):
    def __init__(self):
        super().__init__() ###### Modify this to make it B/W images instead of colored
        layers= [nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,bias=False)]
        #layers= [nn.Conv2d(1, 512, kernel_size=7, stride=2, padding=3,bias=False)] ### Sanity check without resnet34
        #layers += list(models.resnet34(pretrained=True).children())[1:-2]
        layers += list(models.resnet18(pretrained=True).children())[1:4]
        layers += [AdaptiveConcatPool2d(), Flatten()]
        layers += [nn.BatchNorm1d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(128, 64, bias=True), nn.ReLU(inplace=True)]
        layers += [nn.BatchNorm1d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(64, 16, bias=True), nn.ReLU(inplace=True)]
        layers += [nn.Linear(16,2)]
        self.agemodel = nn.Sequential(*layers)
        
        
    def forward(self, x):
        
        x=x.unsqueeze(dim=1)
        return self.agemodel(x).squeeze(-1)
    
    
class ArousalModel2(nn.Module):
    def __init__(self):
        super().__init__() ###### Modify this to make it B/W images instead of colored
        layers= [nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,bias=False)]
        ##layers += list(models.resnet34(pretrained=True).children())[1:-2]
        layers += list(models.resnet18(pretrained=True).children())[1:4]
        layers += [AdaptiveConcatPool2d(), Flatten()]
        layers += [nn.BatchNorm1d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(128, 64, bias=True), nn.ReLU(inplace=True)]
        layers += [nn.BatchNorm1d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)]
        layers += [nn.Dropout(p=0.50)]
        layers += [nn.Linear(64, 16, bias=True), nn.ReLU(inplace=True)]
        
        layers += [nn.Linear(16,2)]
        self.agemodel = nn.Sequential(*layers)
        
        
    def forward(self, x):
        
        x=x.unsqueeze(dim=1)
        return self.agemodel(x).squeeze(-1)
    
val_mod_v2=ValenceModel2()
aro_mod_v2=ArousalModel2()

val_mod_v2.load_state_dict(torch.load('Models/deep_models/val_modelv2.pt'))    
aro_mod_v2.load_state_dict(torch.load('Models/deep_models/aro_modelv2.pt')) 
val_mod_v2.eval()
aro_mod_v2.eval()

    

def compute_mel2(audio_file):
    '''
    spectrogram computations for AV mapping
    '''
    loader = essentia.standard.MonoLoader(filename=audio_file)

    audio = loader()
    #hyperparameters
    w = Windowing(type = 'hann')
    spectrum = Spectrum()  # FFT() would return the complex FFT, here we just want the magnitude spectrum
    mfcc = MFCC(numberCoefficients=10)
    logNorm = UnaryOperator(type='log')

    mfccs = []
    melbands = []
    melbands_log = []

    for frame in FrameGenerator(audio, frameSize=2206, hopSize=1103, startFromZero=True):
        mfcc_bands, _ = mfcc(spectrum(w(frame)))
        #mfccs.append(mfcc_coeffs)
        melbands.append(mfcc_bands)
        melbands_log.append(logNorm(mfcc_bands))

    # transpose to have it in a better shape
    # we need to convert the list to an essentia.array first (== numpy.array of floats)
    #melbands are not used in the beginning but can be useful
    #mfccs = essentia.array(mfccs).T
    #melbands = essentia.array(melbands).T 
    melbands_log = essentia.array(melbands_log).T
    
    return melbands_log

def restructure_mels(mel):
    '''
    restructure the mels for the AV model
    '''
    n_iters=int(2*mel.shape[1]/1799)
    newmels=[]
    
    for i in range(n_iters):
        newmels.append(mel[:,i*899:i*899+1799])
        
    return newmels

def find_valence_arousal(newmels,val_model,aro_model):
    '''
    Performs AV inference
    '''
    
    valence=[]
    arousal=[]
    val_model=val_mod_v2.to(device)

    aro_model=aro_mod_v2.to(device)
    for i in range(len(newmels)):
    
        valence.append(val_model(torch.as_tensor(newmels[i:i+1]).to(device)).detach().cpu().numpy())
        arousal.append(aro_model(torch.as_tensor(newmels[i:i+1]).to(device)).detach().cpu().numpy())
        #print(aro_model(torch.as_tensor(newmels[i:i+1])).detach().numpy())
    
    

    
    return torch.as_tensor(valence).mean(0),torch.as_tensor(arousal).mean(0)

def analyze_emo_track(path,val_mod,aro_mod):
    '''
    analyses a track, AV
    '''
    mel_base=compute_mel2(path)
    mels=restructure_mels(mel_base)
    
    valence,arousal=find_valence_arousal(mels,val_mod,aro_mod)
    
    return valence,arousal

def analyze_directory(path, val_mod,aro_mod,length):
    '''
    analyzes a dir, AV
    '''
    #directory = os.fsencode(path)
    #f_list=list_files(path)
    emos=[]
    noms=[]
    
    for i in tqdm(range(len(f_list))):
        #try:
            
            #newpath=str(path)+'/'+str(file)[2:-1]
        v,a=analyze_emo_track(f_list[i],val_mod,aro_mod)
           
            
        emos.append([v.numpy(),a.numpy()])
            
        
        
    emos=torch.as_tensor(emos).squeeze(dim=2)
    
    
    return emos[:,0,0],emos[:,1,0]


#### Here, the files are processed, 20 by 20 to be able to stop the processes without loosing anything
if len(F_list)>20:

    for i in range(int(len(F_list)/20)-1):
        f_list=F_list[20*i:20*(i+1)]
        LOW_LEVEL_FEATURES=analyze_directory_ll(path)
        deephits=preprocess_dir_hits(path)
        LOW_LEVEL_FEATURES['Deep Hits']=deephits
        Valence,Arousal=analyze_directory(path,val_mod_v2,aro_mod_v2,20)
        LOW_LEVEL_FEATURES['Valence']=Valence
        LOW_LEVEL_FEATURES['Arousal']=Arousal
        try:
            old_features=old_features.append(LOW_LEVEL_FEATURES,sort=False)
            old_features = old_features.reset_index(drop=True)
        except:
            old_features=LOW_LEVEL_FEATURES
        #old_features=old_features.append(LOW_LEVEL_FEATURES,sort=False)
        #old_features = old_features.reset_index(drop=True)
        #old_features.to_pickle('files/FEATURES_SS_LL_EMO') 
        old_features.to_pickle('files/FEATURES_SS_LL_EMO_v2') #v2 is with slower but better extractor 
    f_list=F_list[50*(i+1):]
    LOW_LEVEL_FEATURES=analyze_directory_ll(path)
    deephits=preprocess_dir_hits(path)
    LOW_LEVEL_FEATURES['Deep Hits']=deephits
    Valence,Arousal=analyze_directory(path,val_mod_v2,aro_mod_v2,20)
    LOW_LEVEL_FEATURES['Valence']=Valence
    LOW_LEVEL_FEATURES['Arousal']=Arousal
    try:
        old_features=old_features.append(LOW_LEVEL_FEATURES,sort=False)
        old_features = old_features.reset_index(drop=True)
    except:
        old_features=LOW_LEVEL_FEATURES
    #old_features.to_pickle('files/FEATURES_SS_LL_EMO')
    old_features.to_pickle('files/FEATURES_SS_LL_EMO_v2') #v2 is with slower but better extractor

else:
    f_list=F_list
    LOW_LEVEL_FEATURES=analyze_directory_ll(path)
    deephits=preprocess_dir_hits(path)
    LOW_LEVEL_FEATURES['Deep Hits']=deephits
    Valence,Arousal=analyze_directory(path,val_mod_v2,aro_mod_v2,20)
    LOW_LEVEL_FEATURES['Valence']=Valence
    LOW_LEVEL_FEATURES['Arousal']=Arousal
    try:
        old_features=old_features.append(LOW_LEVEL_FEATURES,sort=False)
        old_features = old_features.reset_index(drop=True)
    except:
        old_features=LOW_LEVEL_FEATURES
    
    #old_features.to_pickle('files/FEATURES_SS_LL_EMO') 
    old_features.to_pickle('files/FEATURES_SS_LL_EMO_v2') #v2 is with slower but better extractor
    

    
    
    
 
        
    
    